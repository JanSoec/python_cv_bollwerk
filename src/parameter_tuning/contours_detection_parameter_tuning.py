import cv2
import numpy as np
import matplotlib.pyplot as plt
from screen_detector import ScreenDetector


def onValueChange(x):
    print(type(x))


screen_detector = ScreenDetector()
# Create track bars in window 'Settings'
cv2.namedWindow('Setting')
cv2.createTrackbar('Canny Thresh0', 'Setting', 130, 500, onValueChange)
cv2.createTrackbar('Canny Thresh1', 'Setting', 140, 1000, onValueChange)
cv2.createTrackbar('BiLateral D', 'Setting', 14, 30, onValueChange)
cv2.createTrackbar('BiLateral Color', 'Setting', 150, 500, onValueChange)
cv2.createTrackbar('BiLateral Space', 'Setting', 150, 500, onValueChange)
cv2.createTrackbar('FindContours Perimeter', 'Setting', 8, 50, onValueChange)

while True:

    canny_thresh0 = cv2.getTrackbarPos('Canny Thresh0', 'Setting')
    canny_thresh1 = cv2.getTrackbarPos('Canny Thresh1', 'Setting')
    bilateral_d = cv2.getTrackbarPos('BiLateral D', 'Setting')
    bilateral_color = cv2.getTrackbarPos('BiLateral Color', 'Setting')
    bilateral_space = cv2.getTrackbarPos('BiLateral Space', 'Setting')
    find_contours_perimeter = cv2.getTrackbarPos('FindContours Perimeter', 'Setting')

    frame = cv2.imread('images//image_sample04.jpg')

    binary_img, bilateral_img = screen_detector.pre_process(
        frame=frame,
        bi_filter_d=bilateral_d,
        bi_filter_color=bilateral_color,
        bi_filter_sigma=bilateral_space,
        canny_thresh0=canny_thresh0,
        canny_thresh1=canny_thresh1)

    sorted_contours = screen_detector.find_screen_contours(binary_img)[0]
    screen_detector.set_contours(sorted_contours, perimeter=find_contours_perimeter)
    cv2.drawContours(frame, sorted_contours, -1, (0, 255, 0), 3)

    cv2.imshow('original', frame)
    cv2.imshow('canny', binary_img)
    cv2.imshow('bifilter', bilateral_img)

    sorted_contours = screen_detector.find_screen_contours(binary_img)[0]
    screen_detector.set_contours(sorted_contours)
    cropped = screen_detector.crop_screen(binary_img)
    cv2.imshow('binary', cropped)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cv2.waitKey(0)
cv2.destroyAllWindows()
