import cv2
import numpy as np
import sys
from matplotlib import pyplot as plt


def nothing(x):
    pass


# Screen Detection algorithm:
# source: https://www.pyimagesearch.com/2014/04/21/building-pokedex-python-finding-game-boy-screen-step-4-6/


cap = cv2.VideoCapture(0)

cv2.namedWindow('Settings')
cv2.createTrackbar('Edge0', 'Settings', 60, 200, nothing)
cv2.createTrackbar('Edge1', 'Settings', 100, 500, nothing)
cv2.createTrackbar('MultPeri', 'Settings', 8, 50, nothing)
cv2.createTrackbar('H', 'Settings', 100, 100, nothing)
cv2.createTrackbar('S', 'Settings', 100, 100, nothing)
cv2.createTrackbar('Closing', 'Settings', 7, 100, nothing)
while True:
    # Get TrackBar values
    valEdge0 = cv2.getTrackbarPos('Edge0', 'Settings')
    valEdge1 = cv2.getTrackbarPos('Edge1', 'Settings')
    valMultPeri = cv2.getTrackbarPos('MultPeri', 'Settings')
    valH = cv2.getTrackbarPos('H', 'Settings')
    valS = cv2.getTrackbarPos('S', 'Settings')
    valClosing = cv2.getTrackbarPos('Closing', 'Settings')
    if valClosing % 2 == 0:
        valClosing -= 1
    if valClosing <= 0:
        valClosing = 1

    # Capture frame-by-frame
    ret, frame = cap.read()
    blur = cv2.GaussianBlur(frame,(9,9),0)
    cv2.imshow('blur',blur)
    hsv = cv2.cvtColor(blur,cv2.COLOR_BGR2HSV_FULL)
    hsv[:, :, 0] =valH
    hsv[:, :, 1] =valS
    cv2.imshow('hsv',hsv)
    #bgrFiltered = cv2.bilateralFilter(hsv, 11, 150, 150)

    thresh = cv2.threshold(hsv, 250, 255, cv2.THRESH_BINARY)[1]
    cv2.imshow('thresh',thresh)
    graythresh = cv2.cvtColor(thresh,cv2.COLOR_BGR2GRAY)
    #bgrFiltered = cv2.bilateralFilter(graythresh, 11, 180, 180)
    cv2.imshow('graythresh',graythresh)
    bgrCanny = cv2.Canny(graythresh, valEdge0, valEdge1)
    cv2.imshow('bgrCanny',bgrCanny)

    # Use EdgeDetection

    # Find all contours on the EdgeDetection Image
    img, contours, hierarchy = cv2.findContours(bgrCanny, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    # Sort all contours on size to get the biggest first
    sortedContours = sorted(contours, key=cv2.contourArea, reverse=True)[:10]
    screenContour = None
    croppedImage = None

    # Loop through all contours that were found
    for c in sortedContours:
        # approximate the contour
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, (valMultPeri/1000) * peri, True)

        # if our approximated contour has four points, then we can assume that we have found our screen
        if len(approx) == 4:
            screenContour = approx

            (x, y, w, h) = cv2.boundingRect(approx)
            aspectRatio = w / float(h)
            outputText = ""
            if x > 20:
                x -= 20
            if y > 20:
                y -= 20
            h += 50
            w += 75
            if 1.0 <= aspectRatio < 1.3:
                outputText = "Screen?"
                croppedImage = frame[y:y + h, x:x + w]
            elif 1.3 <= aspectRatio < 1.7:
                croppedImage = frame[y:y + h, x:x + w]
                outputText = "Screen"
            elif 1.7 <= aspectRatio < 1.78:
                outputText = "16:9 Screen"
                croppedImage = frame[y:y + h, x:x + w]
            cv2.putText(frame, outputText, (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 255), 2)
            # Break as we don't want to detect smaller contours with 4 points
            break

            # Draw everything
            #cv2.contourArea(screenContour)
    cv2.drawContours(frame, screenContour, -1, (0, 255, 0), 5)

    if croppedImage is not None:

        cv2.imshow('cropped', croppedImage)
        #graycrop =  cv2.cvtColor(croppedImage, cv2.COLOR_BGR2GRAY)
        lapcrop = cv2.Laplacian(croppedImage,cv2.CV_8UC4)
        #sobely = cv2.Sobel(graycrop,cv2.CV_16S,0,1,ksize=5)
        #sthresh = cv2.threshold(sobely,0, 255, cv2.THRESH_BINARY|cv2.THRESH_OTSU)
        #cv2.imshow('ys',sthresh)

        #graycrop =  cv2.cvtColor(croppedImage, cv2.COLOR_BGR2GRAY)
        #cropcanny = cv2.Canny(croppedImage, valEdge0, valEdge1)
        cv2.imshow('l',lapcrop)
        # closedlaplace = cv2.morphologyEx(lapcrop, cv2.MORPH_CLOSE, kernel)
        # openlaplace = cv2.morphologyEx(lapcrop, cv2.MORPH_OPEN, kernel)
        # glaplace = cv2.morphologyEx(lapcrop, cv2.MORPH_GRADIENT, kernel)
        # cv2.imshow('c',closedlaplace)
        # cv2.imshow('o',openlaplace)
        # cv2.imshow('g',glaplace)
        #sobely = cv2.Sobel(cropcanny,cv2.CV_8U,0,1,ksize=5)
        #lapcrop = cv2.Laplacian(graycrop,cv2.CV_8U)
        # graylapcrop =cv2.cvtColor(lapcrop,cv2.)
        #cv2.imshow('cropcanny',cropcanny)
        #ysobelFiltered = cv2.bilateralFilter(lapcrop, 11,250, 250)
        # cv2.imshow('lapcrop',lapcrop)


        # sobelx = cv2.Sobel(lapcrop,cv2.CV_8U,1,0,ksize=5)
        # sobely = cv2.Sobel(lapcrop,cv2.CV_8U,0,1,ksize=5)
        #sobelx = cv2.Sobel(lapcrop,cv2.CV_8U,1,0,ksize=7)
        #sobely = cv2.Sobel(lapcrop,cv2.CV_8U,0,1,ksize=7)
        #closedysobel = cv2.morphologyEx(sobely, cv2.MORPH_GRADIENT, kernel)
        # ysobelFiltered = cv2.bilateralFilter(sobely, 11,250, 250)

        #cv2.imshow('x',sobelx)
        #cv2.imshow('y',sobely)
        #cv2.imshow('yf',closedysobel)
        #ysobelFiltered = cv2.bilateralFilter(sobely, 11,250, 250)
        #yblur = cv2.GaussianBlur(sobely,(9,9),0)
        #cv2.imshow('gr',yblur)

        # closedblurcrop = cv2.morphologyEx(ysobelFiltered, cv2.MORPH_CLOSE, kernel)
        # cv2.imshow('close', closedblurcrop)

        # crophsv[0, :, :] = (cv2.getTrackbarPos('H', 'hsv') / 100)
        # crophsv[:, 0, :] = (cv2.getTrackbarPos('S', 'hsv') / 100)

        #  cv2.imshow('crophsv', crophsv)
        #closedcrop = cv2.morphologyEx(croppedImage, cv2.MORPH_CLOSE, kernel)
        #gradient = cv2.morphologyEx(closedcrop, cv2.MORPH_GRADIENT, kernel)
        #closedcv2.cvtColor(closedcrop, cv2.COLOR_GRAY2BGR)
        #bgrcrop = colorCroppedOverlay = cv2.addWeighted(frame, 0.5, croppedImage, 0.5, 0.0)
        #cv2.imshow('gradient', closedcrop)


    cv2.imshow('orginal', frame)
    # Wait for press of q character to exit
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything is done, release the capture
cap.release()
cv2.destroyAllWindows()