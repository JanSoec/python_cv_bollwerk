import cv2


def nothing(x):
    pass


# Screen Detection algorithm:
# source: https://www.pyimagesearch.com/2014/04/21/building-pokedex-python-finding-game-boy-screen-step-4-6/


cap = cv2.VideoCapture(0)

cv2.namedWindow('Settings')
cv2.createTrackbar('Edge0', 'Settings', 60, 200, nothing)
cv2.createTrackbar('Edge1', 'Settings', 100, 500, nothing)
cv2.createTrackbar('MultPeri', 'Settings', 8, 50, nothing)
cv2.createTrackbar('H', 'Settings', 100, 100, nothing)
cv2.createTrackbar('S', 'Settings', 100, 100, nothing)
cv2.createTrackbar('Closing', 'Settings', 7, 100, nothing)
while True:
    # Get TrackBar values
    valEdge0 = cv2.getTrackbarPos('Edge0', 'Settings')
    valEdge1 = cv2.getTrackbarPos('Edge1', 'Settings')
    valMultPeri = cv2.getTrackbarPos('MultPeri', 'Settings')
    valH = cv2.getTrackbarPos('H', 'Settings')
    valS = cv2.getTrackbarPos('S', 'Settings')
    valClosing = cv2.getTrackbarPos('Closing', 'Settings')
    if valClosing % 2 == 0:
        valClosing -= 1
    if valClosing <= 0:
        valClosing = 1

    # Capture frame-by-frame
    ret, frame = cap.read()
    bgrFiltered = cv2.bilateralFilter(frame, 11, 150, 150)
    bgrCanny = cv2.Canny(bgrFiltered, valEdge0, valEdge1)
    #colorCannyOverlay = cv2.addWeighted(frame, 0.5, bgrCanny, 0.5, 0.0)
    #kernel = np.ones((valClosing, valClosing), np.uint8)
    #morphed = cv2.morphologyEx(bgrCanny.copy(), cv2.MORPH_CLOSE, kernel)

    # Use EdgeDetection

    # Find all contours on the EdgeDetection Image
    img, contours, hierarchy = cv2.findContours(bgrCanny, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    # Sort all contours on size to get the biggest first
    sortedContours = sorted(contours, key=cv2.contourArea, reverse=True)[:10]
    screenContour = None
    croppedImage = None

    # Loop through all contours that were found
    for c in sortedContours:
        # approximate the contour
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, (valMultPeri/1000) * peri, True)

        # if our approximated contour has four points, then we can assume that we have found our screen
        if len(approx) == 4:
            screenContour = approx

            (x, y, w, h) = cv2.boundingRect(approx)
            aspectRatio = w / float(h)
            outputText = ""
            if x > 20:
                x -= 20
            if y > 20:
                y -= 20
            h += 40
            w += 40
            if 1.0 <= aspectRatio < 1.3:
                outputText = "Screen?"
                croppedImage = frame[y:y + h, x:x + w]
            elif 1.3 <= aspectRatio < 1.7:
                croppedImage = frame[y:y + h, x:x + w]
                outputText = "Screen"
            elif 1.7 <= aspectRatio < 1.78:
                outputText = "16:9 Screen"
                croppedImage = frame[y:y + h, x:x + w]
            cv2.putText(frame, outputText, (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 255), 2)
            # Break as we don't want to detect smaller contours with 4 points
            break

            # Draw everything
            #cv2.contourArea(screenContour)
    cv2.drawContours(frame, screenContour, -1, (0, 255, 0), 5)

    if croppedImage is not None:
        cv2.imshow('cropped', croppedImage)
        #closedcrop = cv2.morphologyEx(croppedImage, cv2.MORPH_CLOSE, valClosing)
        #closedcv2.cvtColor(closedcrop, cv2.COLOR_GRAY2BGR)
        bgrcrop = colorCroppedOverlay = cv2.addWeighted(frame, 0.5, croppedImage, 0.5, 0.0)
        cv2.imshow('bgrqcropped', bgrcrop)

    cv2.imshow('canny', bgrCanny)
    cv2.imshow('orginal', frame)

    # Wait for press of q character to exit
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything is done, release the capture
cap.release()
cv2.destroyAllWindows()
