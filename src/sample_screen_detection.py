import cv2
import numpy as np
import matplotlib.pyplot as plt
from screen_detector import ScreenDetector


def onValueChange(x):
    print(type(x))


screen_detector = ScreenDetector()
# Create track bars in window 'Settings'
cv2.namedWindow('Setting')
cv2.namedWindow('PreProcessSettings')
cv2.createTrackbar('Canny Thresh0', 'PreProcessSettings', 500, 500, onValueChange)
cv2.createTrackbar('Canny Thresh1', 'PreProcessSettings', 100, 1000, onValueChange)
cv2.createTrackbar('BiLateral D', 'PreProcessSettings', 16, 30, onValueChange)
cv2.createTrackbar('BiLateral Color', 'PreProcessSettings', 100, 500, onValueChange)
cv2.createTrackbar('BiLateral Space', 'PreProcessSettings', 150, 500, onValueChange)
cv2.createTrackbar('FindContours Perimeter', 'Setting', 8, 50, onValueChange)
cv2.createTrackbar('Crop Offset Size', 'Setting', 20, 500, onValueChange)
cv2.createTrackbar('Histogram Thresh', 'Setting', 50, 1000, onValueChange)

canny_thresh0 = cv2.getTrackbarPos('Canny Thresh0', 'PreProcessSettings')
canny_thresh1 = cv2.getTrackbarPos('Canny Thresh1', 'PreProcessSettings')
bilateral_d = cv2.getTrackbarPos('BiLateral D', 'PreProcessSettings')
bilateral_color = cv2.getTrackbarPos('BiLateral Color', 'PreProcessSettings')
bilateral_space = cv2.getTrackbarPos('BiLateral Space', 'PreProcessSettings')
find_contours_perimeter = cv2.getTrackbarPos('FindContours Perimeter', 'Setting')
offset = cv2.getTrackbarPos('Crop Offset Size', 'Setting')
histogram_thresh = cv2.getTrackbarPos('Histogram Thresh', 'Setting')

frame = cv2.imread('images//image_sample04.jpg')

# Nicht anfassen!!!!
blur = cv2.GaussianBlur(frame, (5, 5), 0)
#cv2.imshow('blur', blur)
hsv = cv2.cvtColor(blur, cv2.COLOR_BGR2HSV)
# hsv[:, :, 0] = 0
hsv[:, :, 1] = 0
#cv2.imshow('Hue',hsv)
thresh = cv2.threshold(hsv, 220, 255, cv2.THRESH_BINARY)[1]
#cv2.imshow('threshhold',thresh)
thresh = cv2.erode(thresh, None, iterations=2)
thresh = cv2.dilate(thresh, None, iterations=4)
#cv2.imshow('threshhold',thresh)
# Anfassen!!!!

binary_img, bilateral_img = screen_detector.pre_process(
    frame=frame,
    bi_filter_d=bilateral_d,
    bi_filter_color=bilateral_color,
    bi_filter_sigma=bilateral_space,
    canny_thresh0=canny_thresh0,
    canny_thresh1=canny_thresh1)

sorted_contours = screen_detector.find_screen_contours(binary_img)[0]
thresh_cropped = screen_detector.crop_screen(thresh)

cv2.drawContours(frame, sorted_contours, -1, (0, 255, 0), 3)
#cv2.imshow('bilateral',bilateral_img)
#cv2.imshow('canny',binary_img)
cv2.imshow('Setting', thresh_cropped)
#cv2.imshow('drawed_contours', frame)

hist, bins = np.histogram(thresh_cropped.ravel(), 256, [0, 256])
cropped = None

if hist[255] > histogram_thresh:
    cropped = screen_detector.crop_screen(frame)
if cropped is not None:
    if screen_detector.analyse_cropped_image(cropped, frame):
        cv2.imshow('ThreshCrop', thresh_cropped)
        cv2.imshow('Cropped', cropped)

cv2.waitKey(0)
cv2.destroyAllWindows()
