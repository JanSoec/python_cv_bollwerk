import cv2
import numpy as np
from screen_detector import ScreenDetector

# Configure our capture to stream a video from input device 0
cap = cv2.VideoCapture(0)
# Initialize our ScreenDetector object for later use
screen_detector = ScreenDetector()

# Start the loop, that gets every frame from the camera input stream
while True:
    # Parameters
    canny_thresh0 = 130  # lower Canny Threshold
    canny_thresh1 = 140  # upper Canny Threshold
    bilateral_d = 14  # Bilateral Filter dimension. Higher Value -> More Blur and Performance needed
    bilateral_color = 150  # The amount of Blur of the Bilateral Filter
    find_contours_perimeter = 8  # The perimeter that is used by findContours function
    offset = 20  # How much offset there needs to be, when cropping the image. Needed for Frame Detection

    # Get current frame
    ret, frame = cap.read()

    # Start HSV procedure
    blur = cv2.GaussianBlur(frame, (9, 9), 0)  # Blur for better results
    hsv = cv2.cvtColor(blur, cv2.COLOR_BGR2HSV_FULL)  # Convert to HSV to get the V channel
    hsv[:, :, 0] = 0  # Exclude H (0) and S (1) Channel
    hsv[:, :, 1] = 0
    thresh = cv2.threshold(hsv, 250, 255, cv2.THRESH_BINARY)[1]  # Threshold on HSV to get all bright pixels
    # Stop HSV procedure

    # get preprocessed images
    # Binary_img is the output of Canny Edge Detection
    # Bilateral_img is the output of our Bilateral Filter used before Edge Detection
    binary_img, bilateral_img = screen_detector.pre_process(
        frame=frame,
        bi_filter_d=bilateral_d,
        bi_filter_color=bilateral_color,
        canny_thresh0=canny_thresh0,
        canny_thresh1=canny_thresh1)

    # Get contours and crop hsv image
    sorted_contours = screen_detector.find_screen_contours(binary_img)[0]
    hsv_thresh_cropped = screen_detector.crop_screen(thresh)

    # Create additional images for debugging
    edge_detection_cropped = screen_detector.crop_screen(binary_img)  # This image shows our edge detection on screens
    contour_frame = frame.copy()  # This image shows the findContours Output on our original frame
    cv2.drawContours(contour_frame, sorted_contours, -1, (0,255,0), 3)  # Draw contours onto original frame
    frame_contours = screen_detector.crop_screen(contour_frame)  # crop contours image as well, for debugging

    # Get Histogram of cropped hsv thresh image and check for value
    hist, bins = np.histogram(hsv_thresh_cropped.ravel(), 2, [0, 256])
    cropped = screen_detector.crop_screen(frame)

    # Get Height and width of a cropped image in pixel
    height, width = cropped.shape[:2]
    # if size is less than 5 or there is more than half pixels black -> we stop our procedure on this frame
    if height < 5 or width < 5 or hist[1] < (height * width) / 2:
        cropped = None

    # Look for Frame and Aspect Ratio
    frame_detected = False
    aspect_ratio_detected = False

    # Check if the cropped image is set
    if cropped is not None:
        # Check for Aspect Ratio using the cropped image size
        if screen_detector.check_for_aspect_ratio(cropped):
            aspect_ratio_detected = True
        # Check for Frame using Hough Lines
        if screen_detector.check_for_frame(cropped):
            frame_detected = True

    # Act depending on the results
    if frame_detected and aspect_ratio_detected:
        cv2.putText(frame, 'Screen detected', (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 255), 2)
        cv2.imshow('HSV thresh', hsv_thresh_cropped)
        cv2.imshow('Canny', edge_detection_cropped)
        cv2.imshow('Contours', frame_contours)
    elif aspect_ratio_detected:
        cv2.putText(frame, 'Aspect ratio correct', (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 255), 2)
        cv2.imshow('HSV thresh', hsv_thresh_cropped)
        cv2.imshow('Canny', edge_detection_cropped)
        cv2.imshow('Contours', frame_contours)

    # show the actual stream
    cv2.imshow('Stream', frame)

    # Check for exit
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# release all allocated resources
cap.release()
cv2.destroyAllWindows()
