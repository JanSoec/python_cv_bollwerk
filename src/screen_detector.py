import cv2
import numpy as np
from math import hypot


class ScreenDetector(object):
    def __init__(self):
        self.screen_x = 0
        self.screen_y = 0
        self.screen_w = 1
        self.screen_h = 1
        self.contour = None

    # This functions takes an unchanged frame and prepares it for further usage.
    # It returns a binary canny image and one with bilateral filter applied.
    def pre_process(
            self,
            frame,
            bi_filter_d=14,  # bilateral filter multiplier
            bi_filter_color=150,  # the amount of bilateral blur
            bi_filter_sigma=150,  # bilateral filter sigma -> does not really impact results
            canny_thresh0=130,  # Canny lower threshold
            canny_thresh1=150):  # Canny upper threshold
        bgr_bilateral_filtered = cv2.bilateralFilter(frame, bi_filter_d, bi_filter_color, bi_filter_sigma)
        # gray = cv2.cvtColor(bgr_bilateral_filtered, cv2.COLOR_BGR2GRAY)
        # thresh = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 22)
        canny = cv2.Canny(bgr_bilateral_filtered, canny_thresh0, canny_thresh1)
        return canny, bgr_bilateral_filtered

    # This function uses the cv2.findContours functions to get all contours of a provided image.
    # These contours are sorted by descending size and returned.
    # Additionally the hierarchy is returned.
    def find_screen_contours(self, binary_image):
        # Find all contours on the EdgeDetection Image
        img, contours, hierarchy = cv2.findContours(binary_image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        # Sort all contours on size to get the biggest first
        sorted_contours = sorted(contours, key=cv2.contourArea, reverse=True)[:25]
        self.set_contours(sorted_contours)
        return sorted_contours, hierarchy

    # This function takes contours found by find_screen_contours and looks for the biggest rectangle.
    # If there is a rectangle it saves the coordinates. Those can be used by our crop_screen function.
    def set_contours(self, sorted_contours, perimeter=8, offset=20):
        for c in sorted_contours:
            # approximate the contour
            peri = cv2.arcLength(c, True)
            approx = cv2.approxPolyDP(c, (perimeter / 1000) * peri, True)

            # if our approximated contour has four points, then we can assume that we have found our screen
            if len(approx) == 4:
                self.contour = c
                (x, y, w, h) = cv2.boundingRect(approx)  # get the approximated rectangle

                # check if offset does not move the image out of view and calculate offset
                if x > offset:
                    x = x - offset
                if y > offset:
                    y = y - offset
                h = h + 2 * offset
                w = w + 2 * offset

                # set x, y, w, h values internally
                self.screen_x = x
                self.screen_y = y
                self.screen_w = w
                self.screen_h = h
                break
            else:
                # it's not a rectangle -> reset x, y, h, w values to default
                self.screen_x = 0
                self.screen_y = 0
                self.screen_w = 1
                self.screen_h = 1

    # This function crops any image to previously set contours.
    # The function set_contours needs to be called prior to this function.
    def crop_screen(self, image):
        return image[self.screen_y:self.screen_y + self.screen_h, self.screen_x:self.screen_x + self.screen_w]

    # This function analyses the image provided.
    # Returns true if there might be a screen in the image.
    # Return false if the provided image does not contain a screen.
    def analyse_cropped_image(self, cropped_image, original_image):
        has_frame = self.check_for_frame(cropped_image)  # check for a frame using Hough Lines
        has_aspect_ratio = self.check_for_aspect_ratio(cropped_image)  # check for aspect ratio
        if has_frame and has_aspect_ratio:
            # print('I am sure this is a screen.')
            return True
        elif has_frame:
            # print('There is an object with a frame.')
            return True
        elif has_aspect_ratio:
            # print('There is an object with a correct aspect ratio.')
            return True
        else:
            # print('There is no screen in this image.')
            return False

    # This function checks for an aspect ratio.
    # Returns True if the found aspect ratio is greater than 1.
    # Returns False if the found aspect ratio is bigger than 1.78 which is 16:9 format
    def check_for_aspect_ratio(self, image):
        aspect_ratio = self.screen_w / float(self.screen_h)
        if 1.0 <= aspect_ratio < 1.3:  # Aspect ratio for turned screens
            output_text = "Screen?"
            cv2.putText(image, output_text, (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 11, 0), 2)
            return False
        elif 1.3 <= aspect_ratio < 1.7:  # Aspect ratio for screens more up front
            output_text = "Screen"
            cv2.putText(image, output_text, (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 11, 0), 2)
            return True
        elif 1.7 <= aspect_ratio < 1.78:  # Aspect ratio for screens that are orthogonal to our camera
            output_text = "16:9 Screen"
            cv2.putText(image, output_text, (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 11, 0), 2)
            return True
        return False

    # This Methods checks for a frame on an image containing a screen.
    # It uses HoughLines and compares every line.
    # If it finds a pair of lines that are parallel, then this function returns True
    def check_for_frame(self, image, max_distance=40):
        bgr_bilateral_filtered = cv2.bilateralFilter(image, 15, 150, 150)
        edges = cv2.Canny(bgr_bilateral_filtered, 50, 200)
        lines = cv2.HoughLinesP(edges, 1, np.pi / 180, 100, lines=np.array([]), minLineLength=80, maxLineGap=80)
        # Loop through all Hough Lines and check for distances-
        if lines is not None:
            a, b, c = lines.shape
            for lineA in range(a):
                for lineB in range(a):
                    if lineA != lineB:
                        # line 1
                        point1 = (lines[lineA][0][0], lines[lineA][0][1])
                        point2 = (lines[lineA][0][2], lines[lineA][0][3])

                        # line 2
                        point3 = (lines[lineB][0][0], lines[lineB][0][1])
                        point4 = (lines[lineB][0][2], lines[lineB][0][3])

                        # calculate distances between line1 and line2
                        dist1 = hypot(point1[0] - point3[0], point1[1] - point3[1])
                        dist2 = hypot(point2[0] - point4[0], point2[1] - point4[1])

                        # If the lines are close enough to each other -> return True for a finding a frame.
                        if dist1 < max_distance and dist2 < max_distance:
                            cv2.line(image, point1, point2, (0, 0, 255), 1, cv2.LINE_AA)
                            cv2.line(image, point3, point4, (0, 0, 255), 1, cv2.LINE_AA)
                            # print('Distance Point 1&2: ' + str(dist1))
                            # print('Distance Point 3&4: ' + str(dist2) + '\n')
                            return True

        # Image does not contain an object with a frame.
        return False
