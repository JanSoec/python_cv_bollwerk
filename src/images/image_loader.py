import cv2
import os

image_path = "image_sample01.jpg"
print(os.path.exists(image_path))

image = cv2.imread('image_sample01.jpg')
cv2.imshow('image', image)
cv2.waitKey(0)
cv2.destroyAllWindows()
